package DataStructure.StackCalculator;

import java.util.ArrayList;
import java.util.Stack;

public class Convert {
    private Stack<String> listStack;
    public String[] tokens;
    private ArrayList<String> result;

    Convert(String formula){
        tokens = formula.split(" ");
        listStack = new Stack<>();
        result = new ArrayList<>();
    }

    public static int opWeight(String op){
        if(op.equals("*") || op.equals("/"))
            return 3;
        else if(op.equals("+") || op.equals("-"))
            return 2;
        else if(op.equals("("))
            return 1;
        else
            return -1;
    }

    public void convertPostfix(){
        for(String token : tokens){
            if(isInteger(token))
                result.add(token);
            else{
                if(token.equals("(") || listStack.empty()){
                    listStack.push(token);
                }
                else if(token.equals(")")){
                    String op = listStack.pop();
                    while(!op.equals("(")){
                        result.add(op);
                        op = listStack.pop();
                    }
                }
                else if(opWeight(token) > opWeight(listStack.peek())){
                    listStack.push(token);
                }
                else{
                    result.add(listStack.pop());
                    result.add(token);
                }
            }

        }
        while(!listStack.isEmpty()){
            result.add(listStack.pop());
        }
    }

    public static boolean isInteger(String token) {
        try {
            Integer.parseInt(token);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public ArrayList<String> getResult() {
        return result;
    }
}

