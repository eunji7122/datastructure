package DataStructure.Polynomial;

class Node {
    private int coef;
    private int expo;
    Node nextNode;

    Node(int coef, int expo){
        this.coef = coef;
        this.expo = expo;
        nextNode = null;
    }

    public int getCoef(){
        return coef;
    }

    public int getExpo(){
        return expo;
    }
}