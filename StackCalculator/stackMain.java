package DataStructure.StackCalculator;

import java.util.Scanner;

public class stackMain {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("수식 입력");
        String formula = scan.nextLine();

        Convert convert = new Convert(formula);
        convert.convertPostfix();

        System.out.println(convert.getResult());
        Calculate calculate = new Calculate(convert.getResult());
        System.out.println(calculate.getResult());
    }

}
