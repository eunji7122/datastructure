package DataStructure.Polynomial;

public class Polynomial {
   public Node header;

   Polynomial(){
       header = null;
   }

   public void addData(int coef, int expo){
       Node node = new Node(coef, expo);

       if(header == null){
           header = node;
       }
       else{
           Node current = header;
           while(current.nextNode != null){
               current = current.nextNode;
           }
           current.nextNode = node;
       }
   }

   public void displayPolyList(){
       if(header == null){
           System.out.println("입력된 노드가 없습니다.");
       }
       else{
           Node current = header;
           while(current.nextNode != null){
               System.out.print(current.getCoef() + "x^" + current.getExpo() + " + ");
               current = current.nextNode;
           }
           System.out.print(current.getCoef() + "x^" + current.getExpo());
           System.out.println();
       }
   }
}
