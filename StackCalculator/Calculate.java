package DataStructure.StackCalculator;

import java.util.ArrayList;
import java.util.Stack;

public class Calculate {
    private Stack <String> stack;
    private ArrayList<String> formula;

    Calculate(ArrayList<String> formula){
        this.formula = formula;
        stack = new Stack<>();

        for(String token : formula){
            if(Convert.isInteger(token))
                stack.push(token);
            else{
                String operand1 = stack.pop();
                String operand2 = stack.pop();
                stack.push(String.valueOf(opCalculate(token, operand1, operand2)));
            }
        }
    }

    public static int opCalculate(String op, String operand1, String operand2){
        int op1 = Integer.parseInt(operand1);
        int op2 = Integer.parseInt(operand2);

        if(op.equals("+"))
            return (op1 + op2);
        else if(op.equals("-"))
            return (op1 - op2);
        else if(op.equals("*"))
            return (op1 * op2);
        else
            return (op1 / op2);
    }

    public String getResult() {
        return stack.pop();
    }

}
