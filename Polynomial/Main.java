package DataStructure.Polynomial;

public class Main {
    public static void main(String[] args) {
        Polynomial A = new Polynomial();
        A.addData(4, 3);
        A.addData(3, 2);
        A.addData(5, 1);
        A.displayPolyList();

        Polynomial B = new Polynomial();
        B.addData(4, 3);
        B.addData(3, 2);
        B.addData(5, 1);
        B.displayPolyList();

        AddPolynomial(A, B).displayPolyList();
    }

    static Polynomial AddPolynomial(Polynomial A, Polynomial B){
        Node nodeA = A.header;
        Node nodeB = B.header;
        Polynomial C = new Polynomial();

        while(nodeA != null && nodeB != null){
            if(nodeA.getExpo() > nodeB.getExpo()){
                C.addData(nodeA.getCoef(), nodeA.getExpo());
                nodeA = nodeA.nextNode;
            }
            else if(nodeA.getExpo() < nodeB.getExpo()){
                C.addData(nodeB.getCoef(), nodeB.getExpo());
                nodeB = nodeB.nextNode;
            }
            else{
                C.addData(nodeA.getCoef() + nodeB.getCoef(), nodeA.getExpo());
                nodeA = nodeA.nextNode;
                nodeB = nodeB.nextNode;
            }
        }

        while(nodeA != null){
            C.addData(nodeA.getCoef(), nodeA.getExpo());
            nodeA = nodeA.nextNode;
        }

        while(nodeB != null){
            C.addData(nodeB.getCoef(), nodeB.getExpo());
            nodeB = nodeB.nextNode;
        }

        return C;
    }
}
